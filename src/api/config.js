const baseUrl="https://jsonplaceholder.typicode.com"
 export const GET_POSTS= ()=>`${baseUrl}/posts`;
 export const GET_USERS=()=>`${baseUrl}/users`;
 export const UPDATE_USERS=(postId)=>`${baseUrl}/posts/${postId}`