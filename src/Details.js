import MaterialTable from "material-table";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { GET_POSTS, GET_USERS } from "./api/config";
import DialogComponent from "./components/DialogComponent";
function Details() {
  const [open, setOpen] = useState(false);
  const [obj,setObj]=useState({id:"",title:"",body:"",userId:""})
  const [postDataArray, setPostDataArray] = useState([]);
  const [usersDataArray, setUsersDataArray] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);
  const onClickUpdate = (updatedPostData) => {
    console.log(updatedPostData,"updatedpostData")
    const posts = [...postDataArray];
    const postId = updatedPostData.id;
    posts[postId-1] = updatedPostData;
    setPostDataArray(posts);
  };
  const getUserName = (userId) => {
    const userData = usersDataArray.find((user) => {
      return user.id === userId;
    });
    return userData?userData.name:""
  };
  const fetchData = async () => {
    const postResponse = await axios.get(GET_POSTS(), {
    });
    const usersResponse = await axios.get(GET_USERS(), {
    });
    setPostDataArray(postResponse.data);
    setUsersDataArray(usersResponse.data);
  };
  const columns = [
    {
      title: "Post id",
      field: "id",
    },
    {
      title: "Title",
      field: "title",
    },
    {
      title: "Users",
      field: "userId",
      render:(post)=>  getUserName(post.userId) 
    },

  ];

  return (
    <>
     
      <MaterialTable
        title="Posts"
        columns={columns}
        data={postDataArray}
        options={{ actionsColumnIndex: -1 }}
        actions={[
          {
            icon: "edit",
            tooltip: "Edit Row",
            onClick: (event, rowData) => {
              const {id,title,body,userId}=rowData
              setObj({id,title,body,userId})
              setOpen(true);
            },
          },
        ]}
      />
      <DialogComponent
        open={open}
        setOpen={setOpen}
        onClickUpdate={onClickUpdate}
          obj={obj}
          setObj={setObj}
      />
    </>
  );
}
export default Details;
