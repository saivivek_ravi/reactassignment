import React, { useEffect, useState } from "react";

import {
  TextField,
  DialogActions,
  DialogTitle,
  Dialog,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles({
  root: {
    fontWeight: "bolder",
  },
  title: {
    fontWeight: "bolder",
    color: "#ccc",
  },
  spacing: {
    padding: "1rem 1rem",
    paddingRight: "13rem",
  },
});

export default function DialogComponent({
  open,
  setOpen,
  onClickUpdate,
  obj: { id, title, body, userId },
  setObj,
}) {
  const classes = useStyles();
  const [disabled, setDisabled] = useState(true);
  const [openSnackbar, setOpenSnackBar] = useState(false);
  const handleChange = (e) => {
    setDisabled(false);
    return setObj((prevstate) => {
      return { ...prevstate, [e.target.name]: e.target.value };
    });
  };
  const handleClose = () => {
    setOpen(false);
    setDisabled(true);
  };
  const handleSubmit = () => {
    if (title.length > 0 || body.length > 0) {
      onClickUpdate({ id, title, body, userId });
      setOpen(false);
      setOpenSnackBar(true);
      setTimeout(()=>{
        setOpenSnackBar(false)
      },2000)
      setObj({
        id: "",
        title: "",
        body: "",
        userId: "",
      });
    }
    setDisabled(true);
  };
  const handleCloseSnackbar = () => {
    setOpenSnackBar(false);
  };
  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" className={classes.root}>
          Post Details
        </DialogTitle>
        <div className={classes.spacing}>
          <Typography variant="body2" display="block" className={classes.title}>
            Title
          </Typography>

          <TextField value={title} onChange={handleChange} name="title"  />
        </div>
        <div className={classes.spacing}>
          <Typography variant="body2" display="block" className={classes.title}>
            Body
          </Typography>
          <TextField value={body} onClick={() => handleChange} name="body"  fullWidth style={{minWidth:"400px"}} />
        </div>
        <DialogActions>
          <Button
            autoFocus
            disabled={disabled}
            onClick={handleSubmit}
            color="primary"
            variant="contained"
          >
            update
          </Button>
          <Button
            autoFocus
            onClick={handleClose}
            color="secondary"
            variant="contained"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={openSnackbar}
        onClose={handleClose}
        autoHideDuration={1000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      >
        <Alert onClose={handleCloseSnackbar} severity="success">
          Post Updated Successfully
        </Alert>
      </Snackbar>
    </div>
  );
}
